#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : python port of network
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import numpy
import math
import json

from layers import *


class Network:
    ''' this is a layered learning network
    '''

    def __init__(self, num_values, lr=0.1):
        ''' initialize using learning rate
        This always creates an input layer. Size of that layer is given
        by the parameter `num_values`. The current learning rate can be set
        utilizing the parameter `lr`
        '''
        self.layers = []
        self.layers.append(InputLayer(num_values))
        self.learning_rate = lr


    def append(self, layer: AbstractLayer):
        ''' append a layer
        '''
        if len(self.layers) > 0:
            layer.connect(self.layers[-1])
        self.layers.append(layer)


    def predict(self, inputs):
        ''' predict an output based on the input data
        '''
        values = inputs
        for layer in self.layers:
            values = layer.calc_output(values)

        return values


    def train(self, inputs, targets):
        ''' train the network using input and target values
        '''
        # first, create all layer values with input as first entry
        netvals = []
        netvals.append(inputs)

        for layer in self.layers:
            netvals.append(layer.calc_output(netvals[-1]))

        # calculate error for all layers
        errors = []
        errors.append(targets - netvals[-1])
        for i in range(len(self.layers)-1, -1, -1):
            errors.insert(0, self.layers[i].backpropagate_error(errors[0],netvals[i-1]))

        # Since input itself has no validable error, delete it
        errors.pop(0)

        # at this point, netvals[i] is the input of layer[i] netvals[i+1] it's
        # output. error[i] is it's errors

        # propagate error backwards and change weights
        for i in range(len(self.layers)):
            self.layers[i].train( #for the layer
                errors[i], # errors
                netvals[i+1], # outputs
                netvals[i], # inputs
                self.learning_rate # learning rate
            )

        # network error
        return numpy.sum(numpy.absolute(errors[-1]))/len(errors[-1])


    def save(self, filename):
        ''' save a file
        '''
        data = {
            'layers' : []
        }
        for layer in self.layers:
            data['layers'].append(layer.serialize())

        with open (filename, 'w') as fp:
            json.dump(data, fp, indent=2)


    def from_file(filename,lr=0.1):
        ''' load a file
        '''
        data = {}
        with open (filename) as fp:
            data = json.load(fp)

        layers = data['layers']
        inputlayer = layers.pop(0)
        net = Network(inputlayer['num_values'],lr=lr)

        for element in data['layers']:
            lclass = globals()[element['type']]
            net.layers.append(lclass(data=element))

        return net
