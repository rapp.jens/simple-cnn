# Simple CNN
This is an attempt to create a simple python-only
(not yet) convolutional neural network.

It is basically a layered network which can predict outputs
from inputs. I hope that the api is simple enough to make
really useful networks on tiny machines

## creating a network
Creating a network is basically simple

    import network
    # create the network itself giving numer of input values
    net = network.Network(4)
    # append hidden layer
    net.append(
        network.NeuronLayer(30),
        activation=network.NeuronLayer.ACTIVATION_SIGMOID
    )
    # append output layer
    net.append(
        network.NeuronLayer(5),
        activation=network.NeuronLayer.ACTIVATION_SIGMOID
    )

Next you need to create some input and target data which
are basically arrays of floats. Try not to get zero values.

## get network output
Once you have a network, you can use it to read outputs from it
Therefore you use the function `predict`.

    outputs = network(inputs)
    # if you want to get the neuron with the highest output, use
    label = numpy.argmax(outputs)

## learn values:
This is how to train your network

    for target, inputs in train:
        # prepare output

        td = numpy.zeros(10) + 0.01
        td[target] += 0.99
        td = numpy.array(td, ndmin=2).T

        net.train(inputs, td)

    # after, you can save the network
    net.save(network_filename)


## persisting
It is also possible to save and load networks using json.
To save a network, simply use

    net.save('mynetwork.json')

To load a network, you use the static Network.from_file method

    net = network.Network.from_file(network_filename)
    # next you would prepare your input data
    dataset = load_dataset()
    # and do your prediction
    outputs = net.predict(dataset)

# TODO:
Next work to do is creating more layer types like some pooling layers
and finally, a convolutional layer

Have fun, folks!

