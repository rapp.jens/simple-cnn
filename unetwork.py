#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : micropython version
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import umath as math
import random
import json

from ulab import numpy


def rand_farray(width, height):
    ''' create an array with randomized weights
    '''
    x = numpy.zeros(width * height)
    for i in range(width * height):
        x[i] = float(random.getrandbits(16))/32768.0 - 1.0
    return x.reshape((width,height))


def atof(values):
    ''' create floats from integer values
    '''
    return numpy.vectorize(lambda x: float(x))(values)


def npabs(values):
    ''' replacement vor numpy.absolute
    '''
    return numpy.vectorize(lambda x: abs(x))(values)


class AbstractLayer():
    ''' this abstract class is designed for creating different layer types
    '''
    def __init__(self, num_values=0, data = None):
        ''' just for init logging
        '''
        if data is not None:
            self.num_values = data['num_values']
        else:
            self.num_values = num_values


    def connect(self, preceeding):
        ''' connect with preceeding layer
        '''
        pass


    def calc_output(self, inputs):
        ''' calculate output of the current layer
        '''
        return inputs


    def train(self, error, outputs, inputs, learning_rate):
        ''' train it
        '''
        pass


    def backpropagate_error(self, error):
        ''' propagate the error back for previous layer
        '''
        pass


    def serialize(self):
        ''' serialize network into dictionary
        '''
        return {
            'type' : self.__class__.__name__,
            'num_values' : self.num_values
        }


    def __repr__(self):
        ''' String representation for the layer
        '''
        return self.__class__.__name__ + f' <{self.num_values} values>'



class InputLayer(AbstractLayer):
    ''' an input layer
    '''
    def __init__(self, num_values=0, data = None):
        super().__init__(num_values, data)



class NeuronLayer(AbstractLayer):
    ''' this is a simple layer of neurons which are connected to other layers
    '''

    ACTIVATION_NONE = 'None'
    ACTIVATION_STEP = 'step'
    ACTIVATION_SIGMOID = 'sigmoid'

    activations = {
        'None' : lambda x : x,
        'sigmoid' : lambda x : 1.0 / (1.0 + math.exp(-x)),
        'step' : lambda x : 1 if x > 0 else -1,
    }


    def __init__(self, num_values=0, data=None, activation=None):
        ''' initialize the layer
        '''
        super().__init__(num_values, data)

        if data is not None:
            print(data['type'], data['num_values'])
            self.set_activation(data['activation'])
            w = data['weights']
            preceeding = int(len(w) / self.num_values)
            print(str(len(w)) + ' weights')
            self.weights = numpy.array(w)
        else:
            self.set_activation(activation)
            self.weights = None


    def set_activation(self, activation):
        '''
        set activation function
        '''
        self.af = activation
        if not self.af in NeuronLayer.activations:
            self.af ='None'

        self.activation = numpy.vectorize (NeuronLayer.activations[self.af])


    def connect(self, preceeding):
        ''' connect with preceeding layer
        '''
        if self.weights is None:
            self.weights = rand_farray(self.num_values, preceeding.num_values)


    def calc_output(self, inputs):
        ''' calculate layer output
        '''
        outputs = numpy.dot(self.weights, inputs)
        act = self.activation(outputs)
        return act


    def train(self, error, outputs, inputs, learning_rate):
        ''' train the layer, correct weights
        '''
        output_error = numpy.dot(
            error * outputs * (1.0 - outputs),
            inputs.T
        )

        self.weights += learning_rate * output_error


    def backpropagate_error(self, error):
        ''' calculate the error backwards
        '''
        return numpy.dot(self.weights.T, error)


    def serialize(self):
        data = super().serialize()
        data.update({
            'weights' : self.weights.tolist(),
            'activation' : self.af,
        })
        return data



class Network:
    ''' this is a layered learning network
    '''

    def __init__(self, num_values, lr=0.1):
        ''' initialize using learning rate
        This always creates an input layer. Size of that layer is given
        by the parameter `num_values`. The current learning rate can be set
        utilizing the parameter `lr`
        '''

        self.layers = []
        self.layers.append(InputLayer(num_values))
        self.learning_rate = lr


    def append(self, layer: AbstractLayer):
        ''' append a layer
        '''
        if len(self.layers) > 0:
            layer.connect(self.layers[-1])
        self.layers.append(layer)


    def predict(self, inputs):
        ''' predict an output based on the input data
        '''
        values = inputs
        for layer in self.layers:
            values = layer.calc_output(values)

        return values


    def train(self, inputs, targets):
        ''' train the network using input and target values
        '''
        # first, create all layer values with input as first entry

        netvals = []
        netvals.append(inputs)

        for layer in self.layers:
            netvals.append(layer.calc_output(netvals[-1]))

        # calculate error for all layers
        errors = []
        errors.append(targets - netvals[-1])
        for i in range(len(self.layers)-1, -1, -1):
            errors.insert(0, self.layers[i].backpropagate_error(errors[0]))

        # Since input itself has no validable error, delete it
        errors.pop(0)

        # at this point, netvals[i] is the input of layer[i] netvals[i+1] it's
        # output. error[i] is it's errors

        # propagate error backwards and change weights
        for i in range(len(self.layers)):
            self.layers[i].train( #for the layer
                errors[i], # errors
                netvals[i+1], # outputs
                netvals[i], # inputs
                self.learning_rate # learning rate
            )

        # network error
        return numpy.sum(npabs(errors[-1]))/len(errors[-1])


    def save(self, filename):
        ''' save a file
        '''
        data = {
            'layers' : []
        }
        for layer in self.layers:
            data['layers'].append(layer.serialize())

        with open (filename, 'w') as fp:
            json.dump(data, fp)


    def from_file(filename,lr=0.1):
        ''' load a file
        '''
        data = {}
        with open (filename) as fp:
            data = json.load(fp)

        layers = data['layers']
        inputlayer = layers.pop(0)
        net = Network(inputlayer['num_values'],lr=lr)

        for element in data['layers']:
            lclass = globals()[element['type']]
            net.layers.append(lclass(data=element))

        return net
