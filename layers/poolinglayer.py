#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : python port of network
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import numpy
import math
import json

from . import abstractlayer

class PoolingLayer1D(abstractlayer.AbstractLayer):
    ''' this layer compresses previous layers output.
    It does so by combining outputs with one of several algorithms
    '''

    def __init__(
        self,
        shape=10,
        data = None,
        ):
        ''' initialize the pooling layer
        `source_shape` defines the shape of the source image
        '''
        super().__init__(shape, data)


    def predict(self, inputs):
        ''' compress data according to parameters
        '''
        outputs = numpy.zeros(self.shape)

        step = len(inputs) / self.num_values

        for i in range(self.num_values):
            outputs[i] = max(inputs[i * step:i+1*step])

        return outputs


    def backpropagate_error(self, error, inputs):
        ''' this calculates the error back to the previous layer
        '''
        err = numpy.zeros(self.perceeding.shape)

        step = len(inputs) / len(error)
        for i in range(self.num_values):
            maxval = max(inputs[i * step:i+1*step])
            err[inputs.index(maxval)]=error[i]

        return err


    def connect(self, preceeding):
        ''' check shape of the previous layer
        '''
        super().connect(preceeding)

        if len(self.shape) != len(preceeding.shape):
            raise abstractlayer.LayerException(
                f'Pooling layer shape ({len(self.shape)}) does not match '
                f'Preceeding layer shape ({len(preceeding.shape)})'
            )

        self.factors = [i / j for i, j in zip(preceeding.shape, self.shape)]


class PoolingLayer2D(abstractlayer.AbstractLayer):
    ''' this layer compresses previous layers output.
    It does so by combining outputs with one of several algorithms
    '''

    def __init__(
        self,
        shape=10,
        data = None,
        ):
        ''' initialize the pooling layer
        `source_shape` defines the shape of the source image
        '''
        super().__init__(shape, data)


    def predict(self, inputs):
        ''' compress data according to parameters
        '''
        outputs = numpy.zeroes(self.shape)

        step = len(inputs) / self.num_values

        for i in range(self.num_values):
            outputs[i] = max(inputs[i * step:i+1*step])

        return outputs


    def train(self, inputs, targets):
        ''' this layer does not train, it always returns 0
        '''
        return 0


    def backpropagate_error(self, error, inputs):
        ''' this calculates the error back to the previous layer
        '''
        return error


    def connect(self, preceeding):
        ''' check shape of the previous layer
        '''
        super().connect(preceeding)

        if len(self.shape) != len(preceeding.shape):
            raise abstractlayer.LayerException(
                f'Pooling layer shape ({len(self.shape)}) does not match '
                f'Preceeding layer shape ({len(preceeding.shape)})'
            )

        self.factors = [i / j for i, j in zip(preceeding.shape, self.shape)]
