#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : python port of network
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below

import numpy
import math
import json

from . import abstractlayer


class NeuronLayer(abstractlayer.AbstractLayer):
    ''' this is a simple layer of neurons which are connected to other layers
    '''

    ACTIVATION_NONE = 'None'
    ACTIVATION_STEP = 'step'
    ACTIVATION_SIGMOID = 'sigmoid'
    ACTIVATION_RELU = 'relu'

    activations = {
        'None' : lambda x : x,
        'sigmoid' : lambda x : 1.0 / (1.0 + math.exp(-x)),
        'step' : lambda x : 1 if x > 0 else -1,
        'relu' : lambda x : max ([0,x]),
    }


    def __init__(self, shape=0, data=None, activation=None):
        ''' initialize the layer
        '''
        super().__init__(shape, data)

        if data is not None:
            print(data['type'], data['num_values'])
            self.set_activation(data['activation'])
            w = data['weights']
            preceeding = int(len(w) / self.num_values)
            print(str(len(w)) + ' weights')
            self.weights = numpy.array(w)
        else:
            self.set_activation(activation)
            self.weights = None


    def set_activation(self, activation):
        '''
        set activation function
        '''
        self.af = activation
        if not self.af in NeuronLayer.activations:
            self.af ='None'

        self.activation = numpy.vectorize (NeuronLayer.activations[self.af])


    def connect(self, preceeding):
        '''
        connect with preceeding layer
        '''
        super().connect(preceeding)

        if self.weights is None:
            self.weights = numpy.random.normal(
                0.0,
                pow(self.num_values, -0.5),
                (self.num_values, preceeding.num_values)
            )


    def calc_output(self, inputs):
        ''' calculate layer output
        '''
        outputs = numpy.dot(self.weights, inputs)
        act = self.activation(outputs)
        return act


    def train(self, error, outputs, inputs, learning_rate):
        ''' train the layer, correct weights
        '''

        output_error = numpy.dot(
            error * outputs * (1.0 - outputs),
            numpy.transpose(inputs)
        )
        self.weights += learning_rate * output_error


    def backpropagate_error(self, error, inputs):
        ''' calculate the error backwards
        '''
        return numpy.dot(self.weights.T, error)


    def serialize(self):
        data = super().serialize()
        data.update({
            'weights' : self.weights.tolist(),
            'activation' : self.af,
        })
        return data
