#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : python port of network
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import numpy
import math
import json


class LayerException(Exception):
    pass


class AbstractLayer():
    ''' this abstract class is designed for creating different layer types
    '''
    def __init__(self, shape=0, data = None):
        ''' Set the number of output values for the layer.
        This method defines the number of outputs for the layer.
        if `shape` is a list or tuple, internalnum_values will be defined
        as the product of all elements.
        It also gives the basic data - if possible
        '''
        if data is not None:
            self.num_values = data['num_values']
        else:
            self.shape = shape
            if type(shape) in [int]:
                self.num_values = shape
            elif type(shape) in [tuple, list, numpy.array]:
                nv = 1
                for element in shape:
                    nv *= element
                self.num_values = nv



    def connect(self, preceeding):
        ''' connect with preceeding layer
        '''
        self.preceeding = preceeding


    def calc_output(self, inputs):
        ''' calculate output of the current layer
        '''
        return inputs


    def train(self, error, outputs, inputs, learning_rate):
        ''' train it
        '''
        pass


    def backpropagate_error(self, error, inputs):
        ''' propagate the error back for previous layer
        '''
        return error


    def serialize(self):
        ''' serialize network into dictionary
        '''
        return {
            'type' : self.__class__.__name__,
            'num_values' : self.num_values
        }


    def __repr__(self):
        ''' String representation of the layer
        '''
        return self.__class__.__name__ + f' <{self.num_values} values>'
