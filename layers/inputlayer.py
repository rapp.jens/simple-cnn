#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description : python port of network
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import numpy
import math
import json

from . import abstractlayer


class InputLayer(abstractlayer.AbstractLayer):
    ''' an input layer
    '''
    def __init__(self, shape=0, data = None):
        super().__init__(shape, data)


