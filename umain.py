#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#  project : simple cnn
#  author  : tecdroid
#  date    : 2022-03-01
#
#  description :
#  this is only an example file. won't run on microcontrollers since it needs
#  much of heap space
#
#  run on unix/windows ports with micropython -X heapsize=32M umain.py
#
#  You should have received a LICENSE file shipped with these files.
#  For more information see the documentation below
#
import sys

# since this is only a subset of original numpy
# there are some workarounds in this file
from ulab import numpy

# load the network
import unetwork as network


'''
download from:
https://github.com/makeyourownneuralnetwork/makeyourownneuralnetwork/blob/master/mnist_dataset/mnist_train_100.csv
'''
training_data = 'mnist_train_100.csv'
network_filename = 'umnisttest.json'


def predict(network, dataset):
    '''
    predict data from preloaded dataset
    dataset is an array of tuples (target, [inputs])
    the function also compares the outputs to the target values and calculates
    the network fitness
    '''
    print('predicting')
    hit = 0;
    for target, inputs in dataset:
        outputs = network.predict(inputs)
        label = numpy.argmax(outputs)
        print(f' target: {target}, label: {label}')
        if label == target:
            hit += 1

    print('*' * 40)
    print(f'having {hit} out of {len(dataset)}.')
    print(f'fitness = {hit/len(dataset) * 100.0}')
    print('*' * 40)


def load_dataset(filename):
    ''' load a dataset
    This function takes a filename as parameter and loads it into a dataset
    The file has to be a csv formated file. the first value is the target value,
    the rest of a line are pixels
    '''
    # load file and prepare data
    lines = []
    with open (filename) as fp:
        lines = fp.readlines()

    dataset = []
    for line in lines:
        values = line.split(',')
        id = int(values[0])
        # this is a workaround for numpy.asarray(ndmin=2)
        vals = numpy.array(network.atof(values[1:]))
        img = (numpy.asarray(vals) / 255.0 * 0.99) + 0.01
        dataset.append((id, numpy.array([img]).T))

    return dataset


def testtrain():
    ''' train a network and save its data
    this loads mnist dataset (see above) from Tariq Rashids book
    "make your own neural network"
    '''
    dataset = load_dataset(training_data)

    train = dataset[:80]
    validate = dataset[80:]

    net = network.Network(784, lr=0.3)
    net.append(network.NeuronLayer(
        100,
        activation=network.NeuronLayer.ACTIVATION_SIGMOID)
    )
    net.append(network.NeuronLayer(
        10,
        activation=network.NeuronLayer.ACTIVATION_SIGMOID)
    )

    predict(net, validate)

    print('training...')
    for i in range(30):
        print(f'epoch {i}')

        for target, inputs in train:
            # prepare output

            td = numpy.zeros(10) + 0.01
            td[target] += 0.99
            td = numpy.array([td]).T

            net.train(inputs, td)

    predict(net, validate)

    net.save(network_filename)


def testload():
    ''' load a network and do a predict
    this is how to use a pretrained net.
    '''
    net = network.Network.from_file(network_filename)
    dataset = load_dataset(training_data)

    predict(net, dataset)


def main():
    testtrain()
    testload()


if __name__ == '__main__':
    sys.exit(main())
